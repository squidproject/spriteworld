#include "TextBox.h"

TextBox::TextBox()
{
    rect.x = textrect.x+10;
    rect.y = textrect.y+10;
    inputTextRect.w = textrect.w;
    inputTextRect.h = 28;
    inputTextRect.x = textrect.x;
    inputTextRect.y = textrect.y + 200;

    typingRect.w = inputTextRect.w;
    typingRect.h = inputTextRect.h;
    typingRect.x = inputTextRect.x + 6;
    typingRect.y = inputTextRect.y + 7;

    cursor.defaultPos.x = inputTextRect.x + 8;
    cursor.defaultPos.y = inputTextRect.y + 6;
    cursor.defaultPos.h = 20;
    cursor.defaultPos.w = 3;

    cursor.pos = cursor.defaultPos;
    outputText.clear();
    Active = false;
    Shift  = false;
    crawler = 0;

    font = TTF_OpenFont("art/fonts/VeraMono.ttf", 12);

    if(!font)
    {
        printf("Unable to load font: %s\n", SDL_GetError());
    }

    //ctor
}

TextBox::~TextBox()
{
    //dtor
}

void TextBox::wrap()
{
    int len = in.length();

    if (len < 55 )
    {
        outputText.push_back(in);
    }

    else
    {
        int y, temp=0;
        do
        {
            while(in[y] != '\0' && in[y] != '\n' && in[y]!=NULL && y < 55)
            {
                y++;
            }

            //Save the value of y in temp so that we can revert if need be
            temp=y;

            //Back up until we hit a space if we are at the end of the text line
            if(y=55)
            {
                while (in[y] != ' ' && y > 0)
                {
                    y--;
                }
            }
            //If y is 0, this won't do!  Revert its value from before the while loop
            if(y==0)
            {
                y=temp;
            }



            outputText.push_back(in.substr(0, y));

            in.erase(0,y);
            y=0;
        }
        while(in[0] != '\0' && in[0] != NULL);
        in += '\0';

    }
    crawler = outputText.size();
    return;

}

void TextBox::update(SDL_Surface* Surf_Display, PlayerCharacterPtr player)
{
    //If there is text coming in, add it to outputText wrapped and clear in
    if(!in.empty())
    {
        wrap();
        in.clear();
    }

    //Free textbg
    SDL_FreeSurface(textbg);
    //Load Textbox BMP

    textbg = SDL_LoadBMP("art/textbox.bmp");
    SDL_BlitSurface(textbg, 0, Surf_Display, &textrect);
    SDL_FreeSurface(inputTextbg);
    inputTextbg = SDL_LoadBMP("art/inputbox.bmp");
    SDL_BlitSurface(inputTextbg, 0, Surf_Display, &inputTextRect);

    SDL_Rect temprect = rect;


    //Render each line of outputText
    //First, check to see if it will all fit on one screen
    if (outputText.size() < 9)
        for(unsigned i=0; i < crawler; i++)
    {
        {
            //Free the text surfaces
            SDL_FreeSurface(textSurface[i]);
            textSurface[i] = TTF_RenderText_Shaded(font, outputText.at(i).c_str(), textcolor, bgcolor);
            SDL_BlitSurface(textSurface[i], NULL, Surf_Display, &temprect);
            temprect.y+=20;
        }
    }
    //if it won't then we only display a part of it
    else
    {
        for (unsigned i = (crawler - 9); i < crawler; i++)
        {
            //We need another temporary variable to index textSurface[] since i will be more  than that can hold
            int j = 0;
            SDL_FreeSurface(textSurface[j]);
            textSurface[j] = TTF_RenderText_Shaded(font, outputText.at(i).c_str(), textcolor, bgcolor);
            SDL_BlitSurface(textSurface[j], NULL, Surf_Display, &temprect);
            temprect.y+=20;
            j++;
        }
    }

    //if there is text in the input box, display it

    //if there is more text than the box can fit, scroll it
    if (typing.size() > 55)
    {
        char temp[55];
        typing.copy(temp, 55, (typing.size() - 55));
        inputTextSurface = TTF_RenderText_Shaded(font, temp, textcolor, {15,15,30});
    }
    //Otherwise don't
    else
    {
        inputTextSurface = TTF_RenderText_Shaded(font, typing.c_str(), textcolor, {15,15,30});
    }

    //Draw the input box
    SDL_BlitSurface(inputTextSurface, NULL, Surf_Display, &typingRect);

    //If the input box is active, draw the cursor
    if(Active)
    {
        SDL_FreeSurface(cursor.image);
        cursor.image = SDL_LoadBMP("art/cursor.bmp");
        SDL_BlitSurface(cursor.image, 0, Surf_Display, &cursor.pos);
    }

    return;

}

void TextBox::keyboard(SDL_Event* Event)
{

    if (Event->key.keysym.sym == SDLK_LSHIFT ||
        Event->key.keysym.sym == SDLK_RSHIFT ||
        Event->key.keysym.sym == SDLK_CAPSLOCK)
        {
            Shift = true;
        }

    else
    if(Event->key.keysym.sym == SDLK_BACKSPACE)
    {
        //If there is anything to delete
        if (typing.size() > 0 )
        {
            typing.pop_back();
            //Check to see if the input field is full: if so, don't move the cursor back
            if (typing.size() < 55)
            {
                cursor.pos.x-=7;
            }
        }
        //If there is nothing in the input field, don't do anything
        else
        {
            cursor.pos = cursor.defaultPos;
        }
    }

    else
    if(Event->key.keysym.sym == SDLK_RETURN)
    {
        if(typing.size() > 0)
        {
            in = typing;
            typing.clear();
            cursor.pos = cursor.defaultPos;
        }
        //If enter is hit when the typing is empty, deactivate it
        else
        {
            SDL_EnableUNICODE(0);
            Active = false;
        }
    }

    //If none of the above, grab the Unicode value and add it to tpying
    else
    {
        typing += Event->key.keysym.unicode;
        //Move the cursor, but not if there is more text than room in the field
        if (cursor.pos.x < (textrect.x + textrect.w - 7))
        {
            cursor.pos.x+=7;
        }
    }

}//End function
