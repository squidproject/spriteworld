#ifndef _CAPP_H
    #define _CAPP_H



#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <SDL/SDL.h>
#include "SDL_ttf.h"

#include "PlayerCharacter.h"
#include "Region.h"
#include "TextBox.h"

typedef std::shared_ptr<PlayerCharacter> PlayerCharacterPtr;


class CApp {

    private:

        bool Running;

        SDL_Surface* Surf_Display;

    public:

        CApp();

        int OnExecute();

    public:

        bool OnInit();

        void OnEvent(SDL_Event*, bool*, PlayerCharacterPtr, Region*, TextBox*);

        void OnLoop();

        void OnRender(PlayerCharacterPtr, Region*, TextBox*);

        void OnCleanup(Region*, TextBox*);

};

#endif
