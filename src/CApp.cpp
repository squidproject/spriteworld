#include "CApp.h"
#include <iostream>
#include <memory>
#include <vector>



    CApp::CApp()
    {
        Surf_Display = NULL;

        Running = true;

    }

    int CApp::OnExecute()
    {
        if (OnInit() == false)
        {
            return -1;
        }

        PlayerCharacterPtr player(new PlayerCharacter());
        Region* region = new Region;
        TextBox* textBox = new TextBox;

        player->rect.x = (Surf_Display->w - player->Sprite->w) / 2;
        player->rect.y = (Surf_Display->h - player->Sprite->h) / 2;

        SDL_Event Event;
        while (Running == true)
        {
            while (SDL_PollEvent(&Event))
            {
                OnEvent(&Event, &Running, player, region, textBox);
            }

            OnLoop();



            OnRender(player, region, textBox);



        }

        OnCleanup(region, textBox);

        return 0;
    }

    int main(int argc, char* argv[])
    {
        CApp theApp;
        return theApp.OnExecute();
        return 0;
    }

