#ifndef DIALOG_H
#define DIALOG_H

#include <string>
#include <vector>

#include "SDL/SDL.h"
#include "SDL_ttf.h"

class Button
{
    public:
        SDL_Rect border;
        std::string text;
        SDL_Surface* image;


};

class Dialog
{
    public:
        Dialog();
        virtual ~Dialog();
        SDL_Rect window;
        std::string text;
        SDL_Surface* image;
        Button button[2];

        void confirm(SDL_Surface*, std::string text, std::string positive, std::string negative, bool* query);

    protected:
    private:
};

#endif // DIALOG_H
