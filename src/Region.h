#ifndef REGION_H
#define REGION_H
#include<string>

#include "CApp.h"
#include "SDL/SDL.h"

//Untype this later and fix the circular dependancy in the headers
typedef std::shared_ptr<PlayerCharacter> PlayerCharacterPtr;
//Smallest building block of world
class Tile
{
    public:

        SDL_Rect rect;
        std::string imgName;
        SDL_Surface* img=NULL;
        bool collide;
        Tile();
        virtual ~Tile() {}
    protected:
    private:
};

class OnScreen
{
    public:
        int x=0;
        int y=0;

};


//Largest Tile set
class Region
{
    public:
        Region();
        Tile** tile; // 2 dimensional array containing all tiles in region
        OnScreen onScreen;  //x and y indices of all tiles on screen
        int width; //size of lattitude
        int height; //size of longitude
        virtual ~Region();
        static void moveScreen(SDL_Surface*, Region*, PlayerCharacterPtr);
        static void DrawScreen(SDL_Surface*, Region*);
    protected:
    private:
};






//void getRegion(Region* region);

#endif // REGION_H
