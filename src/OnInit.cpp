#include "CApp.h"

bool CApp::OnInit()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        std::cout << "\nSDL_INIT Failed";
        exit(1);
    }

    if ((Surf_Display = SDL_SetVideoMode(1216, 704, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL)
    {
        std::cout << "\nSDL_SetVideoMode() Failed";
        exit(1);
    }

    //Set the title bar
    SDL_WM_SetCaption("Sprite World", "Sprite World");
    //Initialize TTF
    TTF_Init();


    return true;
}
