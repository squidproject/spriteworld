#include "CApp.h"
#include "SDL_ttf.h"

void CApp::OnRender(PlayerCharacterPtr player, Region* region, TextBox* textBox)
{
    //Clear the screen
    SDL_FillRect(Surf_Display, &player->rect, SDL_MapRGB(Surf_Display->format, 0, 0, 0));

    //Handle player (screen) movement
    region->moveScreen(Surf_Display, region, player);
    //Draw our tiles
    region->DrawScreen(Surf_Display, region);

    //Set the color of the pixel at 0,0,0, which is black/transparent in aseprite, to be transparent on the sprite bmp

    SDL_SetColorKey (player->Sprite, SDL_SRCCOLORKEY, SDL_MapRGB(player->Sprite->format, 0, 0, 0));



    // Draw player sprite
    SDL_BlitSurface(player->Sprite, 0, Surf_Display, &player->rect);

    //Lastly, draw the textbox and render text

    textBox->update(Surf_Display, player);

    // DRAWING ENDS HERE

}



