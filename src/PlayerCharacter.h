#ifndef PLAYERCHARACTER_H
#define PLAYERCHARACTER_H
#include <string>


class PlayerCharacter
{
    public:
        PlayerCharacter();
        SDL_Rect rect;

        int VelX;
        int VelY;

        std::string name;
        SDL_Surface* Sprite;
        virtual ~PlayerCharacter() {}
    protected:
    private:
};

#endif // PLAYERCHARACTER_H

typedef std::shared_ptr<PlayerCharacter> PlayerCharacterPtr;
