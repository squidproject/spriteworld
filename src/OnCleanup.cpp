#include "CApp.h"

void CApp::OnCleanup(Region* region, TextBox* textBox)
{
    delete region;
    delete textBox;

    SDL_Quit();
}
