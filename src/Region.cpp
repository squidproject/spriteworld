#include "CApp.h"
#include "Region.h"


Tile::Tile()
{
    rect.h=64;
    rect.w=64;
    rect.x=0;
    rect.y=0;
    imgName =  "art/greybricks.bmp";
    collide = true;

}

Region::~Region()
{
    //dtor
    delete [] tile;

}


Region::Region()
//Function will eventually get region info from the server.  For now I'll just hardcode a test region
{
    onScreen.x = 9;
    onScreen.y = 4;
    //where in the map we start

    int i,j = 0; //our for loop counters
    int x,y = 0; //our pixel counters in 64

    //region = new Region;
    height=100;
    width=100;

    //Dynamically allocate 110 tile pointers
    tile = new Tile*[150];

    //Dynamically allocate 100 tiles for each pointer, thus creating a 2 dimsional array of 110x110 tiles
    for(i=0;i<150;i++)
    {
        tile[i] = new Tile[150];
    }

    //clean


    for(i=0;i<100;i++)
    {
        for(j=0;j<100;j++)
        {
            tile[i][j].imgName = "art/greybricks.bmp";
            tile[i][j].rect.h = 64;
            tile[i][j].rect.w = 64;
            tile[i][j].collide = false;
        }
    }

    //create an impassible border on top

    for(i=0;i<5;i++)
    {
        for(j=0;j<100;j++)
        {
            tile[i][j].imgName = "art/blackbg.bmp";
            tile[i][j].collide = true;
        }
    }

    //create an impassible border on bottom

    for(i=95;i<105;i++)
    {
        for(j=0;j<100;j++)
        {
            tile[i][j].imgName = "art/blackbg.bmp";
            tile[i][j].collide = true;
        }
    }


    //create an impassible border on the left
        for(i=0;i<100;i++)
        {
            for(j=0;j<10;j++)
            {
                tile[i][j].imgName =  "art/blackbg.bmp";
                tile[i][j].collide = true;
            }
        }

    //create an impassible border on the right
    for(i=0;i<100;i++)
    {
        tile[i][99].imgName = "art/blackbg.bmp";
        tile[i][99].collide = true;
    }
    //create an impassible border down the middle, minus the top and bottom squares and some in the center

    for(i=5;i<95;i++)
    {

        if(i<=40 || i >=50)
        {
            tile[i][50].imgName = "art/blackbg.bmp";
            tile[i][50].collide = true;
        }
    }

    //accessible horizontal collision tester

    for(i=0;i<24;i++)
    {
        tile[85][i].imgName = "art/blackbg.bmp";
        tile[85][i].collide = true;
    }


}

void Region::DrawScreen(SDL_Surface* Surf_Display, Region* region)
{
    //Draw a black rectangle behind our tiles
    SDL_FillRect(Surf_Display, 0, SDL_MapRGB(Surf_Display->format, 0, 0, 0));
    int i,j=0; //Counters for our for loops
    //declare some references so we don't have to use [region->onScreen.x + i] as indeces
    int& x = region->onScreen.x;
    int& y = region->onScreen.y;

    for(i=0;i<11;i++)

    {
        for(j=0;j<19;j++)

        {

            region->tile[y+i][x+j].img = SDL_LoadBMP(region->tile[y + i][x + j].imgName.c_str());
            //Check to make sure our image loaded
            if (!region->tile[y+i][x+j].img)
            {
                printf("\nUnable to load bitmap: %s\n", SDL_GetError());
            }

            SDL_BlitSurface(region->tile[y + i][x +j].img, 0, Surf_Display, &region->tile[y + i][x + j].rect);


        }
    }



}

void Region::moveScreen(SDL_Surface* Surf_Display, Region* region, PlayerCharacterPtr Player)
{
    int i,j=0; //our for loop counters
    //check to see if each arrow key is being held down
    if (Player->VelX== 1 && region->onScreen.x <90) //if right arrow is held down
    {
            //check for collision first
            if(!region->tile[region->onScreen.y + 5][region->onScreen.x + 10].collide)
            {
                region->onScreen.x++;
                //std::cout << region->onScreen.x << "\n";
            }
            else
                {
                    //std::cout << "\n Collide or not working";
                }
    } //endif

    if (Player->VelX== -1 && region->onScreen.x > 0) //if left arrow is held down
    {

        //check for collision first
        if(!region->tile[region->onScreen.y + 5][region->onScreen.x + 8].collide)
        {
            region->onScreen.x--;
            //std::cout << region->onScreen.x << "\n";
        }
    }


    if (Player->VelY == 1 && region->onScreen.y < 90) //if Down arrow is held down
    {
        //check for collision first
        if(!region->tile[region->onScreen.y + 6][region->onScreen.x + 9].collide)
        {
            region->onScreen.y++;
            //std::cout << region->onScreen.y << "\n";
        }
            else
                {
                    //std::cout << "\n Collide or not working";
                }
    }


    if (Player->VelY == -1 && region->onScreen.y > 0 ) //if Up arrow is held down
    {
        //check for collision first
        if(!region->tile[region->onScreen.y + 4][region->onScreen.x + 9].collide)
        {
            region->onScreen.y--;
            //std::cout << region->onScreen.y << "\n";
        }
            else
                {
                    //std::cout << "\n Collide or not working";
                }
    }


    //once the check is complete and x and y changed, we set the location of each onscreen tile, starting at 0,0
    for(i=0;i<11;i++)
    {
        for(j=0;j<19;j++)
        {
            SDL_FreeSurface(region->tile[region->onScreen.y + i][region->onScreen.x + j].img);
            region->tile[region->onScreen.y + i][region->onScreen.x + j].rect.x=j*64;
            region->tile[region->onScreen.y + i][region->onScreen.x + j].rect.y=i*64;

        }

    } //endif

    //Now a delay to control player speed a bit
    SDL_Delay(50);




    return;

} //end moveScreen
