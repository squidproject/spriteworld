#include "CApp.h"
#include <iostream>
#include "Page.h"
#include "PlayerCharacter.h"
#define VEL .5

void CApp::OnEvent(SDL_Event* Event, bool* Running, PlayerCharacterPtr Player, Region* region, TextBox* textBox)
{

    int xvel, yvel;
    switch (Event->type)
    {
        //Handle the fun stuff first: if x is clicked

        case SDL_QUIT:
        {
            *Running = false;
            break;
        }

        //Second:  If the mouse is clicked

        case SDL_MOUSEBUTTONUP:
        {
            if (Event->button.x >=  (textBox->inputTextRect.x ) &&
                Event->button.x <=  (textBox->inputTextRect.x + 400) &&
                Event->button.y >=  (textBox->inputTextRect.y  ) &&
                Event->button.y <=  (textBox->inputTextRect.y + 24 ) &&
                Event->button.button == SDL_BUTTON_LEFT)
                {
                    textBox->Active = true;
                    SDL_EnableUNICODE(1);
                }

            else
            {
                textBox->Active = false;
                SDL_EnableUNICODE(0);
            }

            break;
        }

        case SDL_KEYDOWN:
        {
            //if a key is pressed while textBox->Active is true, call textBox.keyboard to handle it
            if(textBox->Active)
            {
                //First enable Unicode
                textBox->keyboard(Event);
            }

            else
            //If a key is pressed whhile the textbox is NOT active
            {
                switch(Event->key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                    {
                        *Running = false;
                        break;
                    }
                    case SDLK_RETURN:
                    {
                        textBox->Active = true;
                        SDL_EnableUNICODE(1);
                        break;
                    }
                    case SDLK_DOWN:
                    {
                        //std::cout << "\nDown Key Pressed\n";
                        Player->VelY=1;
                        break;
                    }
                    case SDLK_s:
                    {
                        //std::cout << "\nDown Key Pressed\n";
                        Player->VelY=1;
                        break;
                    }
                    case SDLK_UP:
                    {
                        //std::cout << "\nUp Key Pressed\n";
                        Player->VelY=-1;
                        break;
                    }
                    case SDLK_w:
                    {
                        //std::cout << "\nUp Key Pressed\n";
                        Player->VelY=-1;
                        break;
                    }
                    case SDLK_LEFT:
                    {
                        //std::cout << "\nLeft Key Pressed\n";
                        Player->VelX=-1;
                        break;
                    }
                    case SDLK_a:
                    {
                        //std::cout << "\nLeft Key Pressed\n";
                        Player->VelX=-1;
                        break;
                    }
                    case SDLK_RIGHT:
                    {
                        //std::cout << "\nRight Key Pressed\n";
                        Player->VelX=1;
                        break;
                    }
                    case SDLK_d:
                    {
                        //std::cout << "\nRight Key Pressed\n";
                        Player->VelX=1;
                        break;
                    }
                }//end switch(Event->key.keysym.sym)

            }//endelse
            break;
        }//case SDL_KEYDOWN:
        //if a key is released


        case SDL_KEYUP:
        {

                switch(Event->key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                    {
                        *Running = false;
                        break;
                    }

                    case SDLK_DOWN:
                    {
                        //std::cout << "\nDown Key Released\n";
                        Player->VelY=0;
                        break;
                    }
                    case SDLK_s:
                    {
                        //std::cout << "\nDown Key Released\n";
                        Player->VelY=0;
                        break;
                    }
                    case SDLK_UP:
                    {
                        //std::cout << "\nUp Key Released\n";
                        Player->VelY=0;
                        break;
                    }
                    case SDLK_w:
                    {
                        //std::cout << "\nUp Key Released\n";
                        Player->VelY=0;
                        break;
                    }
                    case SDLK_LEFT:
                    {
                        //std::cout << "\nLeft Key Released\n";
                        Player->VelX=0;
                        break;
                    }
                    case SDLK_a:
                    {
                        //std::cout << "\nLeft Key Released\n";
                        Player->VelX=0;
                        break;
                    }
                    case SDLK_RIGHT:
                    {
                        //std::cout << "\nRight Key Released\n";
                        Player->VelX=0;
                        break;
                    }

                    case SDLK_d:
                    {
                        //std::cout << "\nRight Key Released\n";
                        Player->VelX=0;
                        break;
                    }
                }
            }//end extra {}
        }//case SDL_KEYUP
        //break;
    }//end outer switch

//end function







