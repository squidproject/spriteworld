#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <vector>
#include <string>
#include <memory>

#include <SDL/SDL.h>
#include "SDL_ttf.h"

#include "PlayerCharacter.h"




class Cursor
{
    public:
    SDL_Surface* image;
    SDL_Rect defaultPos;
    SDL_Rect pos;
};

class TextBox
{
    public:

        void update(SDL_Surface*, PlayerCharacterPtr);
        TextBox();
        virtual ~TextBox();
        TTF_Font* font=NULL;
        SDL_Surface* textbg=NULL;
        SDL_Surface* inputTextbg=NULL;
        std::string in;
        std::string typing;
        std::vector<std::string> outputText;
        void wrap();
        SDL_Color bgcolor = { 0, 0, 245 };
        SDL_Color textcolor =  {255, 255, 255 };
        SDL_Surface* textSurface[10];
        SDL_Surface* inputTextSurface=NULL;
        SDL_Rect rect;
        SDL_Rect textrect = { 800, 470, 400, 200 };
        SDL_Rect inputTextRect;
        SDL_Rect typingRect;
        int crawler;
        void keyboard(SDL_Event*);
        bool Active;
        bool Shift;
        protected:
        Cursor cursor;


    private:


};



#endif // TEXTBOX_H

